use chrono::{DateTime, Local};
use chrono_locale::LocaleDate;
use directories::ProjectDirs;
use serde::{Deserialize, Serialize};

use std::cell::RefCell;
use std::fs::{File, OpenOptions};
use std::time::Duration;

#[derive(Serialize, Deserialize)]
struct Data {
    date: DateTime<Local>,
    record: Option<Duration>,
}

pub struct AppState {
    data: RefCell<Data>,
    dirs: Option<ProjectDirs>,
}

impl AppState {
    pub fn new() -> Self {
        let dirs = ProjectDirs::from("org", "bdeb-robotique", "accidents");
        let data;
        if let Some(dirs) = &dirs {
            if let Ok(save_file) = File::open(dirs.data_local_dir()) {
                data = bincode::deserialize_from(save_file).unwrap();
            } else {
                data = Data {
                    record: None,
                    date: Local::now(),
                };
            }
        } else {
            data = Data {
                record: None,
                date: Local::now(),
            };
        }
        let data = RefCell::new(data);
        let app = Self { data, dirs };
        app.save();
        app
    }

    pub fn update(&self) {
        let elapsed = self.elapsed();
        {
            let mut data = self.data.borrow_mut();
            if data
                .record
                .as_ref()
                .map_or(true, |record| elapsed > *record)
            {
                data.record = Some(elapsed);
            }
            data.date = Local::now();
        }
        self.save()
    }

    fn save(&self) {
        if let Some(dirs) = &self.dirs {
            if let Ok(save_file) = OpenOptions::new()
                .create(true)
                .write(true)
                .truncate(true)
                .open(dirs.data_local_dir())
            {
                bincode::serialize_into::<_, Data>(save_file, &self.data.borrow()).unwrap();
            }
        }
    }

    pub fn format_date<'a>(&'a self, format: &'a str) -> impl std::fmt::Display + 'a {
        self.data.borrow().date.formatl(format, "fr")
    }

    pub fn elapsed(&self) -> Duration {
        (Local::now() - self.data.borrow().date.clone()).to_std().unwrap()
    }

    pub fn record(&self) -> Option<Duration> {
        self.data.borrow().record.clone()
    }

    pub fn found_dirs(&self) -> bool {
        self.dirs.is_some()
    }
}
