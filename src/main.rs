#![windows_subsystem = "windows"]
//! # Clock Sample
//!
//! This sample demonstrates how to use gtk::timeout_add_seconds to run
//! a periodic task, implementing a clock in this example.

mod state;

use gio::prelude::*;
use gtk::prelude::*;
use pango::{AttrList, Attribute};

use std::env::args;
use std::rc::Rc;

use state::AppState;

fn dialog_flags() -> gtk::DialogFlags {
    let mut dialog_flags = gtk::DialogFlags::MODAL;
    dialog_flags.insert(gtk::DialogFlags::DESTROY_WITH_PARENT);
    dialog_flags
}

fn error(msg: &str, window: &gtk::ApplicationWindow) {
    gtk::MessageDialog::new(
        Some(window),
        dialog_flags(),
        gtk::MessageType::Error,
        gtk::ButtonsType::Close,
        msg,
    )
    .run();
}

fn build_ui(application: &gtk::Application) {
    let window = gtk::ApplicationWindow::new(application);

    window.set_position(gtk::WindowPosition::Center);
    window.set_default_size(260, 40);
    window.set_title("Compteur d'accident en robotique");
    window.set_border_width(100);

    let vbox = gtk::Box::new(gtk::Orientation::Vertical, 50);
    vbox.set_valign(gtk::Align::Center);

    let title = gtk::HeaderBar::new();
    title.set_title(Some("Pas d'accident en Robotique depuis:"));
    window.set_titlebar(Some(&title));

    let since_label_attributes = AttrList::new();
    since_label_attributes.insert(Attribute::new_family("sans").unwrap());
    since_label_attributes.insert(Attribute::new_scale(5.).unwrap());
    let since = gtk::Label::new(None);
    since.set_attributes(Some(&since_label_attributes));

    let progress = gtk::ProgressBar::new();

    let record_label_attributes = AttrList::new();
    record_label_attributes.insert(Attribute::new_family("sans").unwrap());
    record_label_attributes.insert(Attribute::new_scale(1.5).unwrap());
    let record = gtk::Label::new(None);
    record.set_attributes(Some(&record_label_attributes));

    let time_label_attributes = AttrList::new();
    time_label_attributes.insert(Attribute::new_family("sans").unwrap());
    time_label_attributes.insert(Attribute::new_scale(1.5).unwrap());
    let time = gtk::Label::new(None);
    time.set_attributes(Some(&time_label_attributes));

    let button = gtk::Button::new_with_label("Reset");
    button.set_can_default(true);
    button.get_style_context().add_class("destructive-action");

    vbox.pack_start(&since, false, false, 0);
    vbox.pack_start(&time, false, false, 0);
    vbox.pack_start(&progress, false, false, 0);
    vbox.pack_start(&record, false, false, 0);
    title.pack_end(&button);

    window.add(&vbox);

    button.grab_default();

    let state = Rc::new(AppState::new());
    if !state.found_dirs() {
        error(
            "Erreur lors de l'initialisation des directories,\
             la date sera oublié lors de la fermeture",
            &window,
        );
    }

    let mut formatter = timeago::Formatter::with_language(timeago::languages::french::French);
    formatter.ago("");
    let update_since_label = move |state: &AppState| {
        let diff = state.elapsed();
        since.set_text(&format!("{}", formatter.convert(diff)));
    };

    let update_progress = move |state: &AppState| {
        let elapsed = state.elapsed();
        let record = state.record();
        let percent = record.map_or(0., |record| {
            (elapsed.as_secs() as f64 / record.as_secs() as f64).min(1.)
        });
        progress.set_fraction(percent);
    };

    let update_time_label = move |state: &AppState| {
        time.set_text(&format!(
            "Date du dernier incident: {}",
            state.format_date("%A le %e %B à %H:%M")
        ));
    };

    let mut formatter = timeago::Formatter::with_language(timeago::languages::french::French);
    formatter.ago("");
    let update_record_label = move |state: &AppState| {
        record.set_text(&format!(
            "Record: {}",
            state.record().map_or_else(
                || "N/A".to_string(),
                |record_time| formatter.convert(record_time)
            ),
        ));
    };

    update_time_label(&state);
    update_record_label(&state);
    update_since_label(&state);
    update_progress(&state);
    window.show_all();

    let state_bis = state.clone();
    button.connect_clicked(move |_| {
        let dialog = gtk::Dialog::new_with_buttons(
            Some("Confirmer le retour à 0"),
            Some(&window),
            dialog_flags(),
            &[
                ("Annuler", gtk::ResponseType::Cancel),
                ("Confirmer l'effacement", gtk::ResponseType::Accept),
            ],
        );
        dialog.set_default_response(gtk::ResponseType::Accept);
        let label = gtk::Label::new(Some("Voulez-vous vraiment remettre le compteur à 0?"));
        let attributes = AttrList::new();
        attributes.insert(Attribute::new_family("sans").unwrap());
        attributes.insert(Attribute::new_scale(1.5).unwrap());
        label.set_attributes(Some(&attributes));
        dialog.get_content_area().add(&label);

        let response_type = dialog.run();
        if response_type == gtk::ResponseType::Accept {
            state.update();
            update_time_label(&state);
            update_record_label(&state);
        }
        if response_type != gtk::ResponseType::DeleteEvent {
            dialog.destroy();
        }
    });

    // update now and atfterward once every second
    gtk::timeout_add_seconds(1, move || {
        update_since_label(&state_bis);
        update_progress(&state_bis);
        // we could return gtk::Continue(false) to stop our clock after this tick
        gtk::Continue(true)
    });
}

fn main() {
    let application =
        gtk::Application::new(Some("ca.qc.bdeb.robotique.accidents"), Default::default())
            .expect("Initialization failed...");

    application.connect_activate(build_ui);

    application.run(&args().collect::<Vec<_>>());
}
